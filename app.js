require("dotenv").config();
const express = require("express");
const bodyParser = require("body-parser");
const ejs = require("ejs");
const mongoose = require("mongoose");
const passport = require("passport");
const passportLocalMongoose = require("passport-local-mongoose");
const session = require("express-session");
const GoogleStrategy = require("passport-google-oauth20").Strategy;
const FacebookStrategy = require("passport-facebook").Strategy;

// App setup
const app = express();
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static("public"));
app.use(session({
  secret: process.env.SESSION_SECRET,
  resave: false,
  saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());

// Database configurations
mongoose.connect(process.env.DB_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true
});
mongoose.set('useCreateIndex', true);

const userSchema = new mongoose.Schema({
  username: String,
  password: String,
  googleId: String,
  facebookId: String,
  secrets: Array
});

userSchema.plugin(passportLocalMongoose);
const User = mongoose.model('User', userSchema);

// Passport setup
passport.use(User.createStrategy());
passport.serializeUser((user, done) => {
  done(null, user.id);
});
passport.deserializeUser((id, done) => {
  User.findById(id, (err, user) => {
    done(err, user);
  });
});
passport.use(new GoogleStrategy({
    clientID: process.env.GOOGLE_CLIENT_ID,
    clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    callbackURL: process.env.HOST + "/auth/google/secrets",
    userProfileURL: "https://www.googleapis.com/oauth2/v3/userinfo"
  },
  (token, tokenSecret, profile, done) => {
    User.findOne( {googleId : profile.id}, function( err, foundUser ){
    if( !err ){                                                          //Check for any errors
        if( foundUser ){                                          // Check for if we found any users
            return done( null, foundUser );                  //Will return the foundUser
        }else {                                                        //Create a new User
            const newUser = new User({
                googleId : profile.id
            });
            newUser.save( function( err ){
                if(!err){
                    return done(null, newUser);                //return newUser
                }
            });
        }
    }else{
        console.log( err );
    }
   });
  }
));
passport.use(new FacebookStrategy({
    clientID: process.env.FACEBOOK_CLIENT_ID,
    clientSecret: process.env.FACEBOOK_CLIENT_SECRET,
    enableProof: true,
    callbackURL: process.env.HOST + "/auth/facebook/secrets"
  },
  (token, tokenSecret, profile, done) => {
    User.findOne( {facebookId : profile.id}, function( err, foundUser ){
    if( !err ){                                                          //Check for any errors
        if( foundUser ){                                          // Check for if we found any users
            return done( null, foundUser );                  //Will return the foundUser
        }else {                                                        //Create a new User
            const newUser = new User({
                facebookId : profile.id
            });
            newUser.save( function( err ){
                if(!err){
                    return done(null, newUser);                //return newUser
                }
            });
        }
    }else{
        console.log( err );
    }
   });
  }
));

// Aplication pages
app.get("/", (req, res) => {
  if (req.isAuthenticated()) {
    res.redirect("/secrets");
  } else {
    res.render("home");
  }
});

app.get("/login", (req, res) => {
  if (req.isAuthenticated()) {
    res.redirect("/secrets");
  } else {
    res.render("login", { error: false });
  }
});

app.get("/register", (req, res) => {
  if (req.isAuthenticated()) {
    res.redirect("/secrets");
  } else {
    res.render("register", { error: false });
  }
});

app.get("/secrets", async (req, res) => {
  const users = await User.find({});
  const userSecrets = users.map(user => user.secrets);
  const allSecrets = Array.prototype.concat(...userSecrets)
  if (req.isAuthenticated()) {
    res.render("secrets", {
      secrets: allSecrets || []
    });
  } else {
    res.redirect("/login");
  }
});

app.get("/submit", (req, res) => {
  if (req.isAuthenticated()) {
    res.render("submit");
  } else {
    res.redirect("/login");
  }
});
// Application functionalities
app.post("/submit", (req, res) => {
  User.findOneAndUpdate(
   { _id: req.user._id },
   { $push: { secrets: req.body.secret  } },
  function (error, success) {
        if (error) {
            res.redirect("/submit")
        } else {
          res.redirect("/secrets")
        }
    });

});

app.post("/register", (req, res) => {
  User.register({
    username: req.body.username
  }, req.body.password
  , (error, user) => {
    if (error) {
      res.render("register", { error: true });
    } else {
      passport.authenticate("local")(req, res, () => {
        res.redirect("/secrets");
      });
    }
  });
});

app.post("/login", (req, res, next) => {
  const user = new User({
    username: req.body.username,
    password: req.body.password
  });

  passport.authenticate('local', function(err, user, info) {
    if (err) { return next(err); }
    if (!user) { return res.redirect('/register'); }
    req.logIn(user, function(err) {
      if (err) { return next(err); }
      return res.redirect('secrets');
    });
  })(req, res, next);
});

app.get("/logout", (req, res) => {
  req.logout();
  res.redirect("/login");
});

// Google authetication
app.get("/auth/google", passport.authenticate('google', { scope: ['profile'] }));

app.get("/auth/google/secrets", passport.authenticate('google', { failureRedirect: "/login" }), (req, res) => {
  res.redirect("/secrets");
});

// Facebook authentication
app.get('/auth/facebook', passport.authenticate('facebook'));

app.get("/auth/facebook/secrets", passport.authenticate('facebook', { failureRedirect: "/login" }), (req, res) => {
  res.redirect("/secrets");
});

// App listener
app.listen(process.env.PORT, () => {
  console.log("Server started on port " + process.env.PORT);
});
